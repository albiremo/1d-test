import numpy as np
import h5py
from h5py import File as H5File
import os
import matplotlib.pyplot as plt
from remodarco.archer_file import archer_file as caso
from remodarco.archer_plot import archer_plot as aplot
#------------------------------

class archer_average(caso,aplot):
    def __init__(self,casefilename,h5name):
       # super(archer_average, self).__init__(casefilename,h5name)
        super().__init__(casefilename,h5name)
        self.path = "./figure/"
  
    def time_average(self,x,y,z,print_interm,start,name):
        var_aide= np.zeros( (self.nx, self.ny, self.nz) )
        timeold = self.time[0] #number of relaunch
        # in the loop is controlled if the number of the relaunch is equal to the previous
        # it means that we are at the last iteraz of the sequence, so it is the average we want
        for i in range(len(self.relaunch)-1):
         if self.relaunch[i] in start:
            print('skip %s relaunch'%(self.relaunch[i]))
         else:
            timestart = self.time[i-1]
            timeold= self.time[i-1]
            print('start time =', timestart)
            break

        for i in range(len(self.relaunch)-1):
          if self.relaunch[i] in start:
            print('skip %s relaunch'%(self.relaunch[i]))
          else:
            if self.relaunch[i]==self.relaunch[i+1]:
                print("skip %s %s"%(self.relaunch[i],self.iteraz[i]))
            else:
                print("average %s at time %s"%(self.relaunch[i],self.iteraz[i]))
                average_variable = self.read_h5_scalar(self.relaunch[i],self.iteraz[i],name)
                # vof_average collect all the vof_mean of the last iteraz
                var_aide += average_variable*(self.time[i]-timeold)
                print(self.time[i]-timeold)
                timeold =self.time[i]
                # print of different average of the last time step
                if print_interm==1:
                    name_save="average_"+name+"_"+self.relaunch[i]+"_"+self.iteraz[i]
                    if name == "Vof_mean":
                        self.disegna_vof(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
                    else:
                        self.disegna_general(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
        #last add of average
        print("average %s at time %s"%(self.relaunch[-1],self.iteraz[-1]))
        average_variable = self.read_h5_scalar(self.relaunch[-1],self.iteraz[-1],name)
        if print_interm==1:
           name_save="average_"+name+"_"+self.relaunch[-1]+"_"+self.iteraz[-1]
           if name == "Vof_mean":
                self.disegna_vof(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
           else:
                self.disegna_general(z,y,average_variable[self.nx//2,:,:],self.path,name_save)
        var_aide += average_variable*(self.time[-1]-timeold)
        print(self.time[-1]-timeold)
        average_variable = var_aide/(self.time[-1]-timestart)
        return average_variable


    def space_average(self,field):
        #this routine returns the space average in x direction
        field_new = np.zeros( (self.ny,self.nz) )
        for i in range(self.nx):
            field_new += field[i,:,:]
        return field_new/self.nx


