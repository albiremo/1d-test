import numpy as np
import h5py
from h5py import File as H5File
import os
import matplotlib.pyplot as plt
from remodarco.archer_save import archer_save as sauf

#------------------------------
class archer_curvature:
    def __init__(self):


    def extract_pdf(self,x,y,z,pos,box,ninter,window,frac,start):
       for seq in range(len(self.relaunch)-1):
         if self.relaunch[seq] in start:
            print('skip %s relaunch'%(self.relaunch[seq]))
         else:
             k1 = self.read_h5_k1(self.relaunch[seq],self.iteraz[seq])
             k2 = self.read_h5_k2(self.relaunch[seq],self.iteraz[seq])
             if window==0:
                 vof = self.read_h5_vof(self.relaunch[seq],self.iteraz[seq])
                 fig = plt.figure()
                 plt.plot(np.ones(self.ny)*z[pos],y)
                 cs=plt.contourf(z,y,vof[self.nx//2,:,:],levels=self.levels,cmap='jet',extend='both')
                 cs.cmap.set_over('red')
                 cs.changed()
                 cbar=plt.colorbar(cs)
                 cbar.set_ticks(self.ticchi)
                 plt.suptitle('time: %s'%(self.time[seq]))
                 plt.savefig("./curvature/seq_%s.png"%(seq))
                 plt.close(fig)
                 k1_half = k1[:,:,pos]*0.01
                 k2_half = k2[:,:,pos]*0.01 #print archer with this offset
                 minK1 = np.amin(k1_half)
                 maxK1 = np.amax(k1_half)
                 minK2 = np.amin(k2_half)
                 maxK2 = np.amax(k2_half)
                 k1_box = k1[:,:,pos-10:pos+10]*0.01
                 k2_box = k2[:,:,pos-10:pos+10]*0.01
                 print('maxk1= %f, maxk2 = %f, mink1 = %f, mink2 = %f'%(maxK1,maxK2,minK1,minK2))
                 fig = plt.figure()
                 plt.contourf(y,x,k1_half[:,:],200,cmap='jet')
                 plt.colorbar()
                 plt.suptitle('k1 %s %s'%(self.relaunch[seq],self.iteraz[seq]))
                 plt.xlabel('y[m]')
                 plt.ylabel('x[m]')
                 plt.savefig("./curvature/k1_%s_%s_%s.png"%(str(pos),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
                 fig = plt.figure()
                 plt.contourf(y,x,k2_half[:,:],200,cmap='jet')
                 plt.colorbar()
                 plt.suptitle('k2 %s %s'%(self.relaunch[seq],self.iteraz[seq]))
                 plt.xlabel('y[m]')
                 plt.ylabel('x[m]')
                 plt.savefig("./curvature/k2_%s_%s_%s.png"%(str(pos),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
                 fig=plt.figure()
                 n, bins, patches = plt.hist(k1_half, ninter, facecolor='blue', alpha=0.5)
                 plt.savefig("./curvature/histo_%s_%s_%s.png"%(str(pos),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
             else:
                 vof = self.read_h5_vof(self.relaunch[seq],self.iteraz[seq])
                 fig = plt.figure()
                 #pos0=self.get_index(z,0.004)
                 pos0=self.get_index(z,0.004)
                 pos1=self.get_index(z,0)
                 #pos1=self.get_index(z,-0.002)
                 miny=int(round(self.ny*window))
                 maxy=int(round(self.ny*2*window))
                 minx=int(round(self.nx*window))
                 maxx=int(round(self.nx*2*window))
                 #drawing box
                 y_shift=y-0.008
                 z_shift=z+0.008
                 pos1_print=self.get_index(z_shift,0.008)
                 pos0_print=self.get_index(z_shift,0.004) 
                # pos0_print=0
                 vof_print=vof[:,:,::-1]
            #     plt.plot(np.ones(self.ny)*(z_shift[pos0_print]),y_shift)
            #     plt.plot(np.ones(self.ny)*(z_shift[pos1_print]),y_shift)
            #     plt.plot(z_shift,np.ones(self.nz)*(y_shift[miny]))
            #     plt.plot(z_shift,np.ones(self.nz)*(y_shift[maxy]))
                 cs=plt.contourf(z_shift,y_shift,vof_print[self.nx//2,:,:],levels=self.levels,cmap='binary',extend='both')
                 plt.xticks([])
                 plt.yticks([])
    #cs.cmap.set_over('red')
                 #cs.changed()
                 #cbar=plt.colorbar(cs)
                 #cbar.set_ticks(self.ticchi)
             #    plt.suptitle('time: %s'%(self.time[seq]))
                 plt.savefig("./curvature/seq_curv_%s_%s.png"%(self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
                 #define hamf for drawing purpose
                 k1_half = k1[minx:maxx,miny:maxy,pos]*0.01
                 k2_half = k2[minx:maxx,miny:maxy,pos]*0.01 #print archer with this offset
#############################################################
                 minK1 = np.amin(k1_half)
                 maxK1 = np.amax(k1_half)
                 minK2 = np.amin(k2_half)
                 maxK2 = np.amax(k2_half)
############################################################
                 #recast of matrix in an array
                 k1_temp = k1[minx:maxx,miny:maxy,pos1:pos0].reshape((maxx-minx*maxy-miny*pos0-pos1,1))*0.01
                 k2_temp = k2[minx:maxx,miny:maxy,pos1:pos0].reshape((maxx-minx*maxy-miny*pos0-pos1,1))*0.01                  
                 rowk1,colsk1 = np.nonzero(k1_temp)
                 rowk2,colsk2 = np.nonzero(k2_temp)
                 k1_box=k1_temp[rowk1,colsk1]
                 k2_box=k2_temp[rowk2,colsk2]
                 
                 print('maxk1= %f, maxk2 = %f, mink1 = %f, mink2 = %f'%(maxK1,maxK2,minK1,minK2))
                 fig = plt.figure()
                 plt.contourf(y[miny:maxy],x[minx:maxx],k1_half[:,:],200,cmap='jet')
                 plt.colorbar()
                 plt.suptitle('k1 %s %s'%(self.relaunch[seq],self.iteraz[seq]))
                 plt.xlabel('y[m]')
                 plt.ylabel('x[m]')
                 plt.savefig("./curvature/k1_%s_%s_%s_%s.png"%(str(window),str(pos1),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
                 fig = plt.figure()
                 plt.contourf(y[miny:maxy],x[minx:maxx],k2_half[:,:],200,cmap='jet')
                 plt.colorbar()
                 plt.suptitle('k2 %s %s'%(self.relaunch[seq],self.iteraz[seq]))
                 plt.xlabel('y[m]')
                 plt.ylabel('x[m]')
                 plt.savefig("./curvature/k2_%s_%s_%s_%s.png"%(str(window),str(pos1),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
###################################################################################
                 fig=plt.figure()
                 plt.hist(abs(k1_box+k2_box)/self.deltax,bins=ninter,range=(0,np.amax(abs(k1_box+k2_box))/(frac*self.deltax)),facecolor='blue')

                # plt.plot(np.ones(self.ny)*self.deltax,y)
                 # n, bins, patches = plt.hist(k1_box/self.deltax,bins=ninter,range=(np.amin(k1_box)/(frac*self.deltax),np.amax(k1_box)/(frac*self.deltax)),facecolor='blue')
                 # n, bins, patches = plt.hist(k1_box,bins=ninter,range=(np.amin(k1_box)/(frac),np.amax(k1_box)/(frac)),facecolor='blue')
                # plt.xlim(0,100)
                # plt.ylim(0,800)
                 
                 # plt.ylim(-2.0,2.0)
                 plt.xlabel(r'$k/\Delta_{x}$')
                 plt.savefig("./curvature/histo_%s_%s_%s.png"%(str(pos1),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
########################################################################"
                 IRQ=2/(self.deltax*(k1_box+k2_box))
                 print('maxIRQ= %f, minIRQ = %f'%(np.amax(IRQ),np.amin(IRQ)))
                 fig=plt.figure()
                 n,bins,patches=plt.hist(IRQ,bins=500,range=(-100,100),facecolor='blue')
   #range=(np.amin(IRQ)/1000,np.amax(IRQ)/1000),facecolor='blue')
                 plt.axvline(2, color='k', linestyle='dashed', linewidth=1)
                 plt.axvline(-2, color='k', linestyle='dashed', linewidth=1)
                 plt.axvline(0, color='k', linestyle='solid', linewidth=1)
                # plt.plot(np.ones(self.ny)*self.deltax,y)
                 # n, bins, patches = plt.hist(k1_box/self.deltax,bins=ninter,range=(np.amin(k1_box)/(frac*self.deltax),np.amax(k1_box)/(frac*self.deltax)),facecolor='blue')
                 # n, bins, patches = plt.hist(k1_box,bins=ninter,range=(np.amin(k1_box)/(frac),np.amax(k1_box)/(frac)),facecolor='blue')
                 plt.xlim(-25,25)
               #  plt.ylim(0,800)
                 
                 # plt.ylim(-2.0,2.0)
                 plt.xlabel(r'$IRQ_k$')
                                  #integrate
                 bin1_1=self.get_index(bins,-25)
                 bin2_1=self.get_index(bins,-2)
                 bin2_2=self.get_index(bins,2)
                 bin3_2=self.get_index(bins,25)
                 integral1 = sum(np.diff(bins)[bin1_1:bin2_1]*n[bin1_1:bin2_1]) 
                 integral2 = sum(np.diff(bins)[bin2_1:bin2_2]*n[bin2_1:bin2_2]) 
                 integral3 = sum(np.diff(bins)[bin2_2:bin3_2]*n[bin2_2:bin3_2]) 
                 integral_tot=integral1+integral2+integral3
                 perc1=integral1/integral_tot
                 perc2=integral2/integral_tot
                 perc3=integral3/integral_tot
                 print('perc1=%f,perc2=%f,perc3+%f'%(perc1,perc2,perc3))
                 plt.text(0,250,"%f"%perc2,ha="center")
                 plt.text(-10,250,"%f"%perc1,ha="center")
                 plt.text(10,250,"%f"%perc3,ha="center")
                 plt.savefig("./curvature/IRQ_%s_%s_%s.png"%(str(pos1),self.relaunch[seq],self.iteraz[seq]))
                 plt.close(fig)
                 name_txt="./curvature/txt_IRQ_"+str(self.relaunch[seq])+"_"+str(self.iteraz[seq])+"_"+str(pos1_print)+".txt"
                 centers=bins[:-1]+bins[1:]/2
                 with open(name_txt, "w") as txtFile:
                    for i in range(len(n)):
                        print( centers[i], n[i], file=txtFile, sep=',' )
                    print('<3 txt <3')



