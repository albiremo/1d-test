# HOW TO READ THE FILES
To run the notebook:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/albiremo%2F1d-test/master)

Folder named with *nxnynz* refinement

## probe_tele.txt
- column 0: time
- column 1: dtime
- column 2: x probe
- column 3: y probe
- column 4: z probe
- column 5: sign level set (telegraphic signal)
- column 6: vof 
- column 7: LS (only in rake data) 

## probe_grad.txt
- column 0: time
- column 1: dtime
- column 2: x probe
- column 3: y probe
- column 4: z probe
- column 5: grad x level set
- column 6: grad y level set
- column 7: grad z level set
- column 8: grad x vof
- column 9: grad y vof
- column 10: grad z vof
- column 11: nx 
- column 12: ny
- column 13: nz

## probe_vit.txt
- column 0: time
- column 1: dtime
- column 2: x probe
- column 3: y probe
- column 4: z probe
- column 5: u
- column 6: v 
- column 7: w 

## probe_surf.txt
- column 0: time
- column 1: dtime
- column 2: x probe
- column 3: y probe
- column 4: z probe
- column 5: surf
- column 6: k1
- column 7: k2






